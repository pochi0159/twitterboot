package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;


	/*// レコード全件取得(htmlからパラメーターをうまく取得できたとき)
	public List<Message> findAllMessage(int messageUserinfo) {
		if(messageUserinfo == 0) {
		 return messageRepository.findAll();
		}
	return messageRepository.findByMessageUserId(messageUserinfo);
	}*/
	//レコード全件取得
	public List<Message> findAllMessage(String start, String end) {

		if (StringUtils.isEmpty(start)) {
			start = "2020-01-01 00:00:00";

		} else {
			start =  start + " 00:00:00";
		}

		if (StringUtils.isEmpty(end)) {
			Date date = new Date();
			end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		} else {
			end = end + " 23:59:59";
		}
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

			Date startDate = sdFormat.parse(start);
			Date endDate = sdFormat.parse(end);
			return messageRepository.findByMessagesCreatedDateBetween(startDate, endDate);

		} catch (ParseException e) {

			e.printStackTrace();
		}
		return null;
	}

	/*//Controllerで分岐させてtメッセージ取得した際
	public List<Message> findByMessageUserId(int messageUserId) {
		return messageRepository.findByMessageUserId(messageUserId);
	}*/

	// レコード追加
	public void saveMessage(Message message) {
	messageRepository.save(message);
	}

	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	public Message findById(Integer id) {
		Message message = (Message) messageRepository.findById(id).orElse(null);
		return message;
	}

}