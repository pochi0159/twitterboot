package com.example.demo.service;

import java.math.BigInteger;
import java.security.MessageDigest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User findByAccountAndPassword(User user) {
		String encPassword = getSHA256(user.getPassword());

		return userRepository.findByAccountAndPassword(user.getAccount(), encPassword);
	}

	public static String getSHA256(String input){

		String toReturn = null;
		try {
		    MessageDigest digest = MessageDigest.getInstance("SHA-256");
		    digest.reset();
		    digest.update(input.getBytes("utf8"));
		    toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
		    e.printStackTrace();
		}

		return toReturn;
	}

	public void saveUser(User user) {
		String encPassword = getSHA256(user.getPassword());
		user.setPassword(encPassword);
		userRepository.save(user);
	}

	public User findByAccount(String account) {
		return userRepository.findByAccount(account);
	}

}
