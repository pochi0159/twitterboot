package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

	@Query(value= "SELECT * FROM messages INNER JOIN users ON messages.user_id = users.id "
			+ "ORDER BY messages.created_date DESC", nativeQuery = true)
	List<Message> findByMessagesCreatedDateBetween(@Param("start") Date startDate, @Param("end")  Date endDate);

	/*List<Message> findByMessageUserId(int messageUserinfo);*/


}
