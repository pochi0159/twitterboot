package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;

@Controller
public class TopController {

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;

	@GetMapping
	public ModelAndView top() {
	ModelAndView mav = new ModelAndView();
	session = request.getSession();
	User loginUser = (User) session.getAttribute("loginUser");
	/*int messageUserinfo = Integer.parseInt(messageUserId);*/

	// 投稿を全件取得

	String start = request.getParameter("start");
	String end = request.getParameter("end");

	List<Message> messageData = messageService.findAllMessage(start, end);
	List<Comment> commentData = commentService.findAllComment();

	/*// 投稿を全件取得
	List<Message> messageData = messageService.findAllMessage(messageUserinfo);*/


	Message message = new Message();
	mav.setViewName("index");
	mav.addObject("messages", messageData);
	mav.addObject("comments", commentData);
	mav.addObject("messageForm", message);
	mav.addObject("loginUser", loginUser);
	return mav;
	}


	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("messageForm") Message message) {
	session = request.getSession();
	User user = (User) session.getAttribute("loginUser");
	message.setUserId(user.getId());

	// 投稿をテーブルに格納
	messageService.saveMessage(message);
	// rootへリダイレクト
	return new ModelAndView("redirect:/");
	}
	// 投稿を削除
		@PostMapping("/delete/{id}")
		public ModelAndView deleteContent(@PathVariable Integer id) {
			messageService.deleteMessage(id);
			return new ModelAndView("redirect:/");
		}

}
