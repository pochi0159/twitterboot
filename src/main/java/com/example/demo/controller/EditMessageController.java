package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.MessageService;

@Controller
public class EditMessageController {

	@Autowired
	MessageService messageService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;

	@GetMapping("/edit")
	public ModelAndView message() {
		ModelAndView mav = new ModelAndView();
		session = request.getSession();
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.addObject("message", messageService.findById(messageId));
		mav.setViewName("/edit");
		return mav;
	}

	@PutMapping("/editMessage/{id}")
	public ModelAndView addMessage(@PathVariable Integer id, @ModelAttribute("Message") Message message) {
		/*Message message = new Message();*/
		session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		/*message.setId(id);
		message.setTitle(form.getTitle());
		message.setText(form.getText());
		message.setCategory(form.getCategory());*/
		Date date = new Date();
		message.setUpdatedDate(date);

		if(!isValid(errorMessages, message)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("message", message);
			mav.setViewName("/edit");
			return mav;
		}


		//仮実装部分 ログイン機能実装後に実装
		message.setUserId(user.getId());
		messageService.saveMessage(message);
		return new ModelAndView("redirect:/");
	}

	public boolean isValid(List<String> errorMessages, Message message) {

		if(!StringUtils.isBlank(message.getCategory())
				&& message.getCategory().length() > 10) {
			errorMessages.add("カテゴリーは10文字以内で入力してください。");
		}
		if(!StringUtils.isBlank(message.getText()) &&
				message.getText().length() > 1000) {
			errorMessages.add("本文は1000字以内で入力してください。");
		}
		if(StringUtils.isBlank(message.getCategory())) {
			errorMessages.add("カテゴリーを入力してください。");
		}
		if(StringUtils.isBlank(message.getText())) {
			errorMessages.add("本文を入力してください。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}
