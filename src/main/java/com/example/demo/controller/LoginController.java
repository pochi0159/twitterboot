package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;



@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		User user = new User();
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.addObject("userForm", user);
		mav.setViewName("login");
		session.removeAttribute("errorMessages");
		return mav;
	}
	@PostMapping("/login")
	public ModelAndView doLogin(@ModelAttribute("userForm") User user) {

		List<String> errorMessages = new ArrayList<String>();
		User userData = userService.findByAccountAndPassword(user);

		ModelAndView mav = new ModelAndView();
		if(!isValid(user, userData, errorMessages)) {
			/*if(userData.getAccount() != null) {
				mav.addObject("account", userData.getAccount());
			}*/
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/login");
			return mav;
		}
		session.setAttribute("errorMessages", errorMessages);
		session.setAttribute("loginUser", userData);
		/*User loginUser =(User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);*/
		return new ModelAndView("redirect:/");
		}

	public boolean isValid(User user, User userData, List<String> errorMessages) {


		String enteredAccount = user.getAccount();
		String enteredPassword = user.getPassword();

		if(StringUtils.isEmpty(enteredAccount)) {
			errorMessages.add("アカウント名を入力してください");
		}
		if(StringUtils.isEmpty(enteredPassword)) {
			errorMessages.add("パスワードを入力してください");
		}
		if(userData == null &&
				(!StringUtils.isEmpty(enteredAccount) && !StringUtils.isEmpty(enteredPassword))) {
			errorMessages.add("アカウント名またはパスワードが間違っています");
		}
		if(userData != null &&userData.isStopped() == true) {
			errorMessages.add("アカウント名またはパスワードが間違っています");
		}
		if(errorMessages.size() != 0 ) {
			return false;
		}

		return true;
	}
}
